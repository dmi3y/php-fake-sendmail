#!/bin/bash

set -e

script="$HOME/bin/fake_sendmail/sendmail.sh"

mkdir -p $(dirname $script)
tee $script > /dev/null <<'EOT'
#!/bin/bash

dir='/tmp/fake_sendmail'
file=$(date +%Y-%m-%d-%H-%M-%S-%N)'.eml'
mkdir -p $dir
sudo chmod -R 777 $dir
cat > $dir/$file
exit 0
EOT

chmod +x $script
echo "sendmail_path = $script" | sudo tee /etc/php5/conf.d/99-fake-sendmail.ini > /dev/null
sudo service apache2 reload > /dev/null
echo 'Fake sendmail successfully installed'
exit 0
