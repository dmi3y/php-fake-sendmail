# Install

```
#!bash
wget -q -O - https://bitbucket.org/polonskiy/php-fake-sendmail/raw/master/install.sh | bash
```

Show latest email:

```
#!bash
cat $(find /tmp/fake_sendmail/ | sort -r | head -1)
```
